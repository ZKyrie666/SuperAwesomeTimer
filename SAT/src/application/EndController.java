package application;

import java.time.Duration;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import services.TimerWithWait;

public class EndController {
	
	int inputHours; int inputMinutes;
	
	@FXML
	private Pane Pane;
	@FXML
	private TextField Otherminutes;
	@FXML
	private TextField Otherhours;
	@FXML
	private Button Button1minute;
	@FXML
	private Button Button5minute;
	@FXML
	private Button Button10minute;
	@FXML
	private Button ButtonExit;
	@FXML
	private Button Start;
	@FXML
	private Label FaildAlarm;
	@FXML
	private ResourceBundle resources;
	
	public EndController()
	{
		
	}
	@FXML
	private void initialize()
	{
		
	}
	@FXML
	public void Exit()
	{
		Platform.exit();
	}
	@FXML
	public void minutes1()
	{
		Duration alarm = Duration.ofMinutes(1);
		TimerWithWait timer = new TimerWithWait(alarm);
		Main.Close();
		timer.start();
	}
	@FXML
	public void minutes5()
	{
		Duration alarm = Duration.ofMinutes(5);
		TimerWithWait timer = new TimerWithWait(alarm);
		Main.Close();
		timer.start();
	}
	@FXML
	public void minutes10()
	{
		Duration alarm = Duration.ofMinutes(10);
		TimerWithWait timer = new TimerWithWait(alarm);
		Main.Close();
		timer.start();
	}
	@FXML
	public void delayOther()
	{
		try 
		{
			inputHours= Integer.parseInt(Otherhours.getText());
			inputMinutes= Integer.parseInt(Otherminutes.getText());
			
			if (inputMinutes>59)
			{
				throw new NumberFormatException();
			}
			
			Duration alarm = Duration.ofHours(inputHours).plus(Duration.ofMinutes(inputMinutes));
			
			TimerWithWait timer = new TimerWithWait(alarm);
	
			Main.Close();
			timer.start();
			FaildAlarm.setVisible(false);
		}
		catch (Exception e)
		{
			FaildAlarm.setVisible(true);
		}
	}
}
