package application;

import java.io.FileInputStream;
import java.io.IOException;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.fxml.FXMLLoader;

public class Main extends Application {

	private static Stage stage;

	@Override
	public void start(Stage primaryStage) throws IOException {
		stage = primaryStage;
	}

	public static void main(String[] args) {
		launch(args);
	}

	public static void StartScene() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		String fxmlDocPath = "WindowDesigns/StartWindow.fxml";
		FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);

		AnchorPane root = (AnchorPane) loader.load(fxmlStream);

		Scene scene = new Scene(root);

		stage.setScene(scene);
		stage.setTitle("Super Awesome Timer");
		stage.show();
	}

	public static void EndScene() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		String fxmlDocPath = "WindowDesigns/EndWindows.fxml";
		FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);

		Pane root = (Pane) loader.load(fxmlStream);

		Scene scene = new Scene(root);

		stage.setScene(scene);
		
		
		stage.setTitle("Super Awesome Timer");
		stage.show();
		lock();
	}

	public void PasswordScene() throws IOException {

	}
	
	private static void lock() throws IOException {
		ProcessBuilder builder = new ProcessBuilder(
				"cmd.exe",
				"/c",
				"cd C:\\Windows\\System32 && rundll32.exe user32.dll,LockWorkStation");
		builder.start();
	}

	public static void Close() {
		stage.close();
	}
}
