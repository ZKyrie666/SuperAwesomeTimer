package application;

import services.TimerWithWait;

import java.time.Duration;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class StartController {
	
	int inputHours; int inputMinutes;
	int inputWarningHours; int inputWarningMinutes;
	boolean inputCheckbox;
	@FXML
	private AnchorPane Pane;
	@FXML
	private TextField inputTextHours;
	@FXML
	private TextField inputTextMinutes;
	@FXML
	private Label FailedAlarm;
	@FXML
	private TextField inputTextWarningHours;
	@FXML
	private TextField inputTextWarningMinutes;
	@FXML
	private Button startButton;
	@FXML
	private PasswordField inputPasswordField;
	@FXML
	private ResourceBundle resources;
	
	public StartController()
	{
		
	}
	@FXML
	private void initialize()
	{
		
	}
	@FXML
	public void printInput()
	{
		try
		{
			inputHours = parseInt(inputTextHours.getText());	
			inputMinutes = parseInt(inputTextMinutes.getText());
			
			inputWarningHours = parseInt(inputTextWarningHours.getText());
			inputWarningMinutes = parseInt(inputTextWarningMinutes.getText());
			
			Duration alarm = Duration.ofHours(inputHours).plus(Duration.ofMinutes(inputMinutes));
			Duration warning = Duration.ofHours(inputWarningHours).plus(Duration.ofMinutes(inputWarningMinutes));
			
			if(alarm.isZero()) {
				FailedAlarm.setText("Alarm is Zero");
				FailedAlarm.setVisible(true);
			}
			else if(warning.equals(alarm)) {
				FailedAlarm.setText("Warning and alarm are equal");
				FailedAlarm.setVisible(true);
			}
			else if(warning.compareTo(alarm) > 0) {
				FailedAlarm.setText("Alarm is less than warning");
				FailedAlarm.setVisible(true);
			}
			else {
				
				TimerWithWait timer;
				
				if(warning.isZero()) {
					timer = new TimerWithWait(alarm);
				}
				else {
					timer = new TimerWithWait(warning, alarm);
				}
				
				Main.Close();
				FailedAlarm.setVisible(false);
				timer.start();
			}		
		}
		catch (NumberFormatException e)
		{
			FailedAlarm.setText("Invalid number format");
			FailedAlarm.setVisible(true);
		}
	}
	
	private int parseInt(String input) throws NumberFormatException {
		if(input.equals("")) {
			return 0;
		}
		else {
			return Integer.parseInt(input);
		}
	}

}
