package application;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class EndWindow extends Application {
	
	private static Stage stage;
	
	@Override
	public void start(Stage secondaryStage) throws IOException {
		
		stage = secondaryStage;
		
		FXMLLoader loader = new FXMLLoader();
		String fxmlDocPath = "WindowDesigns/EndWindows.fxml";
		FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
		
		Pane root = (Pane) loader.load(fxmlStream);
		
		Scene scene = new Scene(root);
		
		secondaryStage.setScene(scene);
		secondaryStage.setFullScreen(true);
		secondaryStage.setTitle("SAT 2.0");
		secondaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
	public static void Close()
	{
		stage.close();
	}
	public static void Open()
	{
		stage.show();
	}
	
}
