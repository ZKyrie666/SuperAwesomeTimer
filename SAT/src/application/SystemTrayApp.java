package application;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import javafx.application.Platform;
import services.TextToSpeech;
import services.TimerWithWait;

public class SystemTrayApp {
	
	private static TrayIcon icon;
	
	public static void main(String[] args) throws IOException, AWTException {
		
		Platform.setImplicitExit(false);
				
		if(SystemTray.isSupported()) {
        	
        	SystemTray tray = SystemTray.getSystemTray();
        	
        	BufferedImage image = ImageIO.read(new File("images/clock-google-web.png")); 
        	icon = new TrayIcon(image);
        	icon.setImageAutoSize(true);
        	icon.setPopupMenu(configurePupupMenu());
        	icon.setToolTip(TimerWithWait.remainingTime(false));
        	
        	addMouseEvents();
        	
        	tray.add(icon);
        	
        	Main.main(null);
        }
        else {
        	System.out.println("SystemTray is not supported!");
        }
	}

	private static void addMouseEvents() {
		icon.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (arg0.getButton() == MouseEvent.BUTTON1)
				{
					icon.displayMessage(TimerWithWait.remainingTime(false), "", MessageType.NONE);
					TextToSpeech tts = new TextToSpeech(TimerWithWait.remainingTime(true));
					tts.speak();
				}
			}
		});
		
		icon.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent arg0) {
				icon.setToolTip(TimerWithWait.remainingTime(false));
			}
			
			@Override
			public void mouseDragged(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public static void setEnablePopupMenu(boolean enable) {
		icon.getPopupMenu().setEnabled(enable);
	}
	
	private static PopupMenu configurePupupMenu() {
		
		PopupMenu menu = new PopupMenu();
		
		MenuItem exit = new MenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		MenuItem timer = new MenuItem("Timer");
		timer.addActionListener(event -> Platform.runLater(new Runnable() {

			@Override
			public void run() {
				try {
					Main.StartScene();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}));
		
		menu.add(exit);
		menu.add(timer);
		
		return menu;
	}
}
