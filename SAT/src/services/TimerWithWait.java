package services;

import java.io.IOException;
import java.time.Duration;

import application.Main;
import application.SystemTrayApp;
import javafx.application.Platform;

public class TimerWithWait {
	
	private static long endTime;
	private Duration durationTillWarning;
	private Duration durationTillAlarm;
	private boolean needWarning;
	private static boolean timerIsRunning;
	
	public TimerWithWait(Duration duration) {
		durationTillAlarm = duration;
		needWarning = false;
		timerIsRunning = false;
	}
	
	public TimerWithWait(Duration warning, Duration alarm) {
		durationTillWarning = alarm.minus(warning);
		durationTillAlarm = alarm;
		needWarning = true;
		timerIsRunning = false;
	}
	
	public static String remainingTime(boolean usedForTTS) {
		if(!timerIsRunning) {
			return "There's no running Timer at this moment";
		}
		else {
			long s = (endTime - System.currentTimeMillis()) / 1000;
			long hours = s / 3600;
			long minutes = (s % 3600) / 60;
			long seconds = s % 60;
			
			if(usedForTTS) {
				
				if(hours == 0 && minutes == 0) {
					return String.format(" %d seconds left", seconds);
				}
				else if(hours == 0) {
					return String.format("%d minutes and %d seconds left", minutes, seconds);
				}
				else {
					return String.format("%d hours and %d minutes left", hours, minutes);
				}
			}
			else {
				return String.format("Remaining Time: %d:%02d:%02d", hours, minutes, seconds);
			}
		}
	}
	
	public void start() {
		timerIsRunning = true;
		endTime = System.currentTimeMillis() + durationTillAlarm.toMillis();
		
		SystemTrayApp.setEnablePopupMenu(false);
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run(){
				try {
					synchronized (this) {
						if (needWarning) {
							wait(durationTillWarning.toMillis());
							warning();
							wait(durationTillAlarm.toMillis() - durationTillWarning.toMillis());
						} else {
							wait(durationTillAlarm.toMillis());
						}
						alarm();
						timerIsRunning = false;
						SystemTrayApp.setEnablePopupMenu(true);
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		});
		
		thread.start();	
	}
	
	private void alarm() {
			Platform.runLater(new Runnable() {

				@Override
				public void run() {
					try {
						Main.EndScene();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			});
	}
	
	private void warning() {
		TextToSpeech tts = new TextToSpeech("Warning:" + remainingTime(true));
		tts.speak();
	}
}

